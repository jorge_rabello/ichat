package com.jorgerabellodev.ichat.app;

import android.app.Application;
import com.jorgerabellodev.ichat.component.ChatComponent;
import com.jorgerabellodev.ichat.component.DaggerChatComponent;
import com.jorgerabellodev.ichat.module.ChatModule;

public class ChatApplication extends Application {

  private ChatComponent component;

  @Override
  public void onCreate() {
    super.onCreate();
    component = DaggerChatComponent.builder()
        .chatModule(new ChatModule(this))
        .build();
  }

  public ChatComponent getComponent() {
    return component;
  }

}
