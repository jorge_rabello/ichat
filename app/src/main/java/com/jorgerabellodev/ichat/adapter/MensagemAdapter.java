package com.jorgerabellodev.ichat.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.jorgerabellodev.ichat.R;
import com.jorgerabellodev.ichat.modelo.Mensagem;
import com.squareup.picasso.Picasso;
import java.util.List;
import javax.inject.Inject;

public class MensagemAdapter extends BaseAdapter {

  private Activity activity;
  private List<Mensagem> mensagens;
  private int idDoCliente;

  @BindView(R.id.tv_texto)
  TextView texto;

  @BindView(R.id.iv_avatar_mensagem)
  ImageView avatar;

  @Inject
  Picasso picasso;

  public MensagemAdapter(int idDoCliente, List<Mensagem> mensagens, Activity activity) {
    this.mensagens = mensagens;
    this.activity = activity;
    this.idDoCliente = idDoCliente;
  }

  @Override
  public int getCount() {
    return mensagens.size();
  }

  @Override
  public Object getItem(int position) {
    return mensagens.get(position);
  }

  @Override
  public long getItemId(int position) {
    return position;
  }

  @Override
  public View getView(int position, View convertView, ViewGroup parent) {

    View linha = activity.getLayoutInflater().inflate(R.layout.mensagem, parent, false);

    ButterKnife.bind(this, linha);

    Mensagem mensagem = (Mensagem) getItem(position);
    int idDaMensagem = mensagem.getId();

    picasso
        .with(activity)
        .load("https://api.adorable.io/avatars/285/" + idDaMensagem + ".png")
        .into(avatar);

    // diferencia cores das linhas recebido/enviado
    if (idDoCliente != mensagem.getId()) {
      linha.setBackgroundColor(Color.CYAN);
    }

    texto.setText(mensagem.getTexto());

    return linha;

  }
}
