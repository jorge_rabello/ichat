package com.jorgerabellodev.ichat.module;

import android.app.Application;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;
import com.jorgerabellodev.ichat.service.ChatService;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Picasso.Builder;
import dagger.Module;
import dagger.Provides;
import org.greenrobot.eventbus.EventBus;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ChatModule {

  private Application app;

  public ChatModule(Application app) {
    this.app = app;
  }

  @Provides
  public ChatService getChatService() {
    Retrofit retrofit = new Retrofit.Builder()
        .baseUrl("https://ichatj.herokuapp.com/")
        .addConverterFactory(GsonConverterFactory.create())
        .build();

    ChatService chatService = retrofit.create(ChatService.class);
    return chatService;
  }

  @Provides
  public Picasso getPicasso() {
    Picasso picasso = new Builder(app).build();
    return picasso;
  }

  @Provides
  public EventBus getEventBus() {
    EventBus eventBus = EventBus.builder().build();
    return eventBus;
  }

  @Provides
  public InputMethodManager getInputMethodManager() {
    InputMethodManager inputMethodManager = (InputMethodManager) app
        .getSystemService(Context.INPUT_METHOD_SERVICE);
    return inputMethodManager;
  }
}
