package com.jorgerabellodev.ichat.event;

import com.jorgerabellodev.ichat.modelo.Mensagem;

public class MensagemEvent {

  public Mensagem mensagem;

  public MensagemEvent(Mensagem mensagem) {
    this.mensagem = mensagem;
  }
}
