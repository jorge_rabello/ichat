package com.jorgerabellodev.ichat.callback;

import android.content.Context;
import com.jorgerabellodev.ichat.event.FailureEvent;
import com.jorgerabellodev.ichat.event.MensagemEvent;
import com.jorgerabellodev.ichat.modelo.Mensagem;
import org.greenrobot.eventbus.EventBus;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OuvirMensagemCallback implements Callback<Mensagem> {

  private Context context;
  private final EventBus eventBus;

  public OuvirMensagemCallback(EventBus eventBus, Context context) {
    this.eventBus = eventBus;
    this.context = context;
  }

  @Override
  public void onResponse(Call<Mensagem> call, Response<Mensagem> response) {

    if (response.isSuccessful()) {
      Mensagem mensagem = response.body();
      eventBus.post(new MensagemEvent(mensagem));
    }
  }

  @Override
  public void onFailure(Call<Mensagem> call, Throwable t) {
    eventBus.post(new FailureEvent());
  }
}
