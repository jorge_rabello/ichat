package com.jorgerabellodev.ichat.component;

import com.jorgerabellodev.ichat.activity.MainActivity;
import com.jorgerabellodev.ichat.adapter.MensagemAdapter;
import com.jorgerabellodev.ichat.module.ChatModule;
import dagger.Component;

@Component(modules = ChatModule.class)
public interface ChatComponent {

  void inject(MainActivity activity);

  void inject(MensagemAdapter adapter);

}
