package com.jorgerabellodev.ichat.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.jorgerabellodev.ichat.R;
import com.jorgerabellodev.ichat.adapter.MensagemAdapter;
import com.jorgerabellodev.ichat.app.ChatApplication;
import com.jorgerabellodev.ichat.callback.EnviarMensagemCallback;
import com.jorgerabellodev.ichat.callback.OuvirMensagemCallback;
import com.jorgerabellodev.ichat.component.ChatComponent;
import com.jorgerabellodev.ichat.event.FailureEvent;
import com.jorgerabellodev.ichat.event.MensagemEvent;
import com.jorgerabellodev.ichat.modelo.Mensagem;
import com.jorgerabellodev.ichat.service.ChatService;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.inject.Inject;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import retrofit2.Call;

public class MainActivity extends AppCompatActivity {

  private int idDoCliente = new Random().nextInt();

  @BindView(R.id.et_texto)
  EditText editText;

  @BindView(R.id.btn_enviar)
  Button button;

  @BindView(R.id.lv_mensagens)
  ListView listaDeMensagens;

  @BindView(R.id.iv_avatar_usuario)
  ImageView avatar;

  List<Mensagem> mensagens;

  @Inject
  ChatService chatService;

  @Inject
  Picasso picasso;

  @Inject
  EventBus eventBus;

  @Inject
  InputMethodManager inputMethodManager;

  private ChatComponent component;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    ButterKnife.bind(this);

    this.picasso.with(this)
        .load("https://api.adorable.io/avatars/285/" + idDoCliente + ".png")
        .into(avatar);

    ChatApplication app = (ChatApplication) getApplication();
    component = app.getComponent();
    component.inject(this);

    if (savedInstanceState != null) {
      mensagens = (List<Mensagem>) savedInstanceState.getSerializable("mensagens");
    } else {
      mensagens = new ArrayList<>();
    }

    MensagemAdapter adapter = new MensagemAdapter(idDoCliente, mensagens, this);
    listaDeMensagens.setAdapter(adapter);

    Call<Mensagem> call = chatService.ouvirMensagens();
    call.enqueue(new OuvirMensagemCallback(eventBus, this));

    eventBus.register(this);

  }


  @OnClick(R.id.btn_enviar)
  public void enviarMensagem() {
    chatService.enviar(new Mensagem(idDoCliente, editText.getText().toString()))
        .enqueue(new EnviarMensagemCallback());

    editText.getText().clear();

    inputMethodManager = (InputMethodManager) getSystemService(
        INPUT_METHOD_SERVICE);
    inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
  }

  @Subscribe
  public void colocaNaLista(MensagemEvent mensagemEvent) {
    mensagens.add(mensagemEvent.mensagem);
    MensagemAdapter adapter = new MensagemAdapter(idDoCliente, mensagens, this);

    listaDeMensagens.setAdapter(adapter);

  }

  @Subscribe
  public void ouvirMensagem(MensagemEvent mensagemEvent) {
    Call<Mensagem> call = chatService.ouvirMensagens();
    call.enqueue(new OuvirMensagemCallback(eventBus, this));
  }

  @Subscribe
  public void lidarCom(FailureEvent failureEvent) {
    Log.e("ERRO", "Falha ao enviar a mensagem !");
    ouvirMensagem(null);
  }

  @Override
  public void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putSerializable("mensagens", (ArrayList<Mensagem>) mensagens);
  }


  @Override
  protected void onStop() {
    super.onStop();
    eventBus.unregister(this);
  }
}
